-- 1. �������� ������
-- ����� ������� � ���������
Select @@SERVERNAME as [Server\Instance];
-- ����� SQL Server
Select @@VERSION as SQLServerVersion;
-- �������� SQL Server
Select @@ServiceName AS ServiceInstance;
-- ������� �� (��, � �������� ��� ���������� �����)
Select DB_NAME() AS CurrentDB_Name;

-- ��� ���������� ����������� �������
SELECT @@Servername AS ServerName ,
	create_date AS ServerStarted ,
	DATEDIFF(s, create_date, GETDATE()) / 86400.0 AS DaysRunning ,
	DATEDIFF(s, create_date, GETDATE()) AS SecondsRunnig
FROM sys.databases
WHERE name = 'tempdb';

-- ������ ��� ����� ����������� �� �����, ��� �� ���'������ � ������ ���������
EXEC sp_helpserver;
--OR
EXEC sp_linkedservers;
--OR
SELECT @@SERVERNAME AS Server ,
	Server_Id AS LinkedServerID ,
	name AS LinkedServer ,
	Product , Provider , Data_Source ,
	Modify_Date
FROM sys.servers
ORDER BY name;

-- ������ ��� ��� �����
EXEC sp_helpdb;
--OR
EXEC sp_Databases;
--OR
SELECT @@SERVERNAME AS Server ,
	name AS DBName ,
	recovery_model_Desc AS RecoveryModel ,
	Compatibility_level AS CompatiblityLevel ,
	create_date , state_desc
FROM sys.databases
ORDER BY Name;
--OR
SELECT @@SERVERNAME AS Server ,
	d.name AS DBName , create_date ,
	compatibility_level , m.physical_name AS FileName
FROM sys.databases d
	JOIN sys.master_files m ON d.database_id = m.database_id
WHERE m.[type] = 0 -- data files only
ORDER BY d.name;


-- 2. �������� ���� �����
use Shops

SELECT * FROM sys.objects
WHERE type = 'U';

EXEC sp_Helpfile;
--OR
SELECT @@Servername AS Server ,
	DB_NAME() AS DB_Name , File_id ,
	Type_desc , Name ,
	LEFT(Physical_Name, 1) AS Drive ,
	Physical_Name ,
	RIGHT(physical_name, 3) AS Ext ,
	Size , Growth
FROM sys.database_files
ORDER BY File_id;-- 3. �������� �������EXEC sp_tables; -- ��� ����� ������� � �������, � �����������
--OR
SELECT @@Servername AS ServerName,
	TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'BASE TABLE'
ORDER BY TABLE_NAME;
--OR
SELECT @@Servername AS ServerName, DB_NAME() AS DBName,
	o.name AS 'TableName' , o.[Type] , o.create_date
FROM sys.objects o
WHERE o.Type = 'U' -- User table
ORDER BY o.name;
--OR
SELECT @@Servername AS ServerName,
	DB_NAME() AS DBName, t.Name AS TableName,
	t.[Type], t.create_date
FROM sys.tables t
ORDER BY t.Name;

-- ʳ������ ������ � ��������
SELECT 'Select ''' + DB_NAME() + '.' + SCHEMA_NAME(SCHEMA_ID) + '.'
	+ LEFT(o.name, 128) + ''' as DBName, count(*) as Count From ' +
	SCHEMA_NAME(SCHEMA_ID) + '.' + o.name
	+ ';' AS ' Script generator to get counts for all tables'
FROM sys.objects o
WHERE o.[type] = 'U'
ORDER BY o.name;-- ���������� � �������� ��SELECT @@ServerName AS ServerName,
	DB_NAME() AS DBName, OBJECT_NAME(ddius.object_id) AS TableName,
	SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups) AS Reads,
	SUM(ddius.user_updates) AS Writes,
	SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups
		+ ddius.user_updates) AS [Reads&Writes],
	( SELECT DATEDIFF(s, create_date, GETDATE()) / 86400.0
	  FROM master.sys.databases
	  WHERE name = 'tempdb'
	) AS SampleDays,
	( SELECT DATEDIFF(s, create_date, GETDATE()) AS SecoundsRunnig
	  FROM master.sys.databases
	  WHERE name = 'tempdb'
	) AS SampleSeconds
FROM sys.dm_db_index_usage_stats ddius
	INNER JOIN sys.indexes i ON ddius.object_id = i.object_id
	AND i.index_id = ddius.index_id
WHERE OBJECTPROPERTY(ddius.object_id, 'IsUserTable') = 1
	AND ddius.database_id = DB_ID()
GROUP BY OBJECT_NAME(ddius.object_id)
ORDER BY [Reads&Writes] DESC;-- 4. �������� �����������SELECT @@Servername AS ServerName, DB_NAME() AS DBName,
	o.name AS ViewName, o.[Type], o.create_date
FROM sys.objects o
WHERE o.[Type] = 'V' -- View
ORDER BY o.NAME
--OR
SELECT @@Servername AS ServerName, DB_NAME() AS DBName,
	Name AS ViewName, create_date
FROM sys.Views
ORDER BY Name
--OR
SELECT @@Servername AS ServerName , TABLE_CATALOG ,
	TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'VIEW'
ORDER BY TABLE_NAME
--OR
SELECT @@Servername AS ServerName, DB_NAME() AS DB_Name,
	o.name AS 'ViewName', o.Type,
	o.create_date, sm.[DEFINITION] AS 'View script'
FROM sys.objects o
	INNER JOIN sys.sql_modules sm ON o.object_id = sm.OBJECT_ID
WHERE o.Type = 'V' -- View
ORDER BY o.NAME;-- 5. �������� ���������, �� �����������SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	o.name AS StoredProcedureName , o.[Type] , o.create_date
FROM sys.objects o
WHERE o.[Type] = 'P' -- Stored Procedures
ORDER BY o.name
--OR
SELECT @@Servername AS ServerName , DB_NAME() AS DB_Name ,
	o.name AS 'StoredProcedureName' , o.[type] ,
	o.Create_date , sm.[definition] AS 'Stored Procedure script'
FROM sys.objects o
	INNER JOIN sys.sql_modules sm ON o.object_id = sm.object_id
WHERE o.[type] = 'P' -- Stored Procedures
 -- AND sm.[definition] LIKE '%insert%'
 -- AND sm.[definition] LIKE '%update%'
 -- AND sm.[definition] LIKE '%delete%'
 -- AND sm.[definition] LIKE '%tablename%'
ORDER BY o.name;-- 6. �������� �������SELECT @@Servername AS ServerName, DB_NAME() AS DB_Name,
	o.name AS 'Functions', o.[Type], o.create_date
FROM sys.objects o
WHERE o.Type = 'FN' -- Function
ORDER BY o.NAME;
--OR
-- ��������� ���������� ��� �������
SELECT @@Servername AS ServerName, DB_NAME() AS DB_Name,
	o.name AS 'FunctionName', o.[type], o.create_date,
	sm.[DEFINITION] AS 'Function script'
FROM sys.objects o
	INNER JOIN sys.sql_modules sm ON o.object_id = sm.OBJECT_ID
WHERE o.[Type] = 'FN' -- Function
ORDER BY o.NAME;-- 7. �������� �������SELECT @@Servername AS ServerName, DB_NAME() AS DBName,
	parent.name AS TableName, o.name AS TriggerName,
	o.[Type], o.create_date
FROM sys.objects o
	INNER JOIN sys.objects parent ON o.parent_object_id = parent.object_id
WHERE o.Type = 'TR' -- Triggers
ORDER BY parent.name, o.NAME
--OR
SELECT @@Servername AS ServerName, DB_NAME() AS DB_Name,
	Parent_id, name AS TriggerName, create_date
FROM sys.triggers
WHERE parent_class = 1
ORDER BY name;
--OR
-- ��������� ���������� �� ��������
SELECT @@Servername AS ServerName, DB_NAME() AS DB_Name,
	OBJECT_NAME(Parent_object_id) AS TableName, o.name AS 'TriggerName',
	o.Type, o.create_date, sm.[DEFINITION] AS 'Trigger script'
FROM sys.objects o
	INNER JOIN sys.sql_modules sm ON o.object_id = sm.OBJECT_ID
WHERE o.Type = 'TR' -- Triggers
ORDER BY o.NAME;-- 8. �������� check � ���������SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	parent.name AS 'TableName' , o.name AS 'Constraints' ,
	o.[Type] , o.create_date
FROM sys.objects o
	INNER JOIN sys.objects parent ON o.parent_object_id = parent.object_id
WHERE o.Type = 'C' -- Check Constraints
ORDER BY parent.name, o.name
--OR
SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	OBJECT_SCHEMA_NAME(parent_object_id) AS SchemaName ,
	OBJECT_NAME(parent_object_id) AS TableName ,
	parent_column_id AS Column_NBR ,
	Name AS CheckConstraintName ,
	type , type_desc , create_date ,
	OBJECT_DEFINITION(object_id) AS CheckConstraintDefinition
FROM sys.Check_constraints
ORDER BY TableName, SchemaName, Column_NBR -- 9. �������� ������ �����SELECT @@Servername AS Server , DB_NAME() AS DBName ,
	isc.Table_Name AS TableName , isc.Table_Schema AS SchemaName ,
	Ordinal_Position AS Ord , Column_Name , Data_Type ,
	Numeric_Precision AS Prec , Numeric_Scale AS Scale ,
	Character_Maximum_Length AS LEN , -- -1 means MAX like Varchar(MAX)
	Is_Nullable , Column_Default , Table_Type
FROM INFORMATION_SCHEMA.COLUMNS isc
	INNER JOIN information_schema.tables ist ON isc.table_name = ist.table_name
-- WHERE Table_Type = 'BASE TABLE' -- 'Base Table' or 'View'
ORDER BY DBName, TableName, SchemaName, Ordinal_position;

-- ����� �������� �� ������� �������
-- ��������������� ��� ������ ���������� �������� � ������ ������ �����
SELECT @@Servername AS Server , DB_NAME() AS DBName ,
	Column_Name , Data_Type , Numeric_Precision AS Prec ,
	Numeric_Scale AS Scale , Character_Maximum_Length ,
	COUNT(*) AS Count
FROM information_schema.columns isc
	INNER JOIN information_schema.tables ist ON isc.table_name = ist.table_name
WHERE Table_type = 'BASE TABLE'
GROUP BY Column_Name, Data_Type, Numeric_Precision,
	Numeric_Scale, Character_Maximum_Length;

--���������� �� ����� �����, �� ����������������
SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	Data_Type , Numeric_Precision AS Prec ,
	Numeric_Scale AS Scale ,
	Character_Maximum_Length AS [Length] , COUNT(*) AS COUNT
FROM information_schema.columns isc
	INNER JOIN information_schema.tables ist ON isc.table_name = ist.table_name
WHERE Table_type = 'BASE TABLE'
GROUP BY Data_Type, Numeric_Precision, Numeric_Scale, Character_Maximum_Length
ORDER BY Data_Type, Numeric_Precision, Numeric_Scale, Character_Maximum_Length

-- ������� ������� �� ������ ���� ������������ � ����� "online"
SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	isc.Table_Name , Ordinal_Position AS Ord , Column_Name ,
	Data_Type AS BLOB_Data_Type , Numeric_Precision AS Prec ,
	Numeric_Scale AS Scale , Character_Maximum_Length AS [Length]
FROM information_schema.columns isc
	INNER JOIN information_schema.tables ist ON isc.table_name = ist.table_name
WHERE Table_type = 'BASE TABLE' AND ( Data_Type IN ( 'text', 'ntext', 'image', 'XML' )
	OR ( Data_Type IN ( 'varchar', 'nvarchar', 'varbinary' )
	AND Character_Maximum_Length = -1 ) ) -- varchar(max), nvarchar(max), varbinary(max)
ORDER BY isc.Table_Name, Ordinal_position;

-- ����������� �������
SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	OBJECT_SCHEMA_NAME(object_id) AS SchemaName ,
	OBJECT_NAME(object_id) AS Tablename , Column_id ,
	Name AS Computed_Column , [Definition] , is_persisted
FROM sys.computed_columns
ORDER BY SchemaName, Tablename, [Definition];
--OR
SELECT @@Servername AS ServerName , DB_NAME() AS DBName ,
	OBJECT_SCHEMA_NAME(t.object_id) AS SchemaName,
	t.Name AS TableName , c.Column_ID AS Ord ,
	c.Name AS Computed_Column
FROM sys.Tables t
	INNER JOIN sys.Columns c ON t.object_id = c.object_id
WHERE is_computed = 1
ORDER BY t.Name, SchemaName, c.Column_ID